// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebaseConfig : {
    apiKey: "AIzaSyDw3A7XHWLBa23StraEUpQ20Egih6oWCmA",
    authDomain: "woven-surface-246415.firebaseapp.com",
    databaseURL: "https://woven-surface-246415.firebaseio.com",
    projectId: "woven-surface-246415",
    storageBucket: "",
    messagingSenderId: "401991999034",
    appId: "1:401991999034:web:1ba9957d41fe6275"},
  
  production: false

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
