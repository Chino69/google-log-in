import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from "@ionic-native/facebook/ngx"


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage {

  

  constructor(private afAuth: AngularFireAuth, 
              private gplus: GooglePlus, private facebook: Facebook, private router: Router) {

                this.afAuth.authState.subscribe(
                  async user => {
                    if(user !== null){
                      this.router.navigate(['/home'])
                    }else{
            
                    }
            
                  } 
            
                )
  }
  async nativeGoogleLogin(): Promise<any> {
    try {
  
      const gplusUser = await this.gplus.login({
        'webClientId': '401991999034-2nob0gf9e9vej5mafhlhkjl5fb7clke6.apps.googleusercontent.com',
                        
        'offline': false,
      })
  
      return await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken))
   
    } catch(err) {
      console.log(err)
    }
    
    
  }
  facebookLogin(): Promise<any> {
    return this.facebook.login(['email', 'public_profile'])
      .then( response => {
        const facebookCredential = firebase.auth.FacebookAuthProvider
          .credential(response.authResponse.accessToken);    
  firebase.auth().signInWithCredential(facebookCredential)
    .then( success => {
       console.log("Firebase success: " + JSON.stringify(success));
       
     });
  }).catch((error) => {
      console.log(error);
      
     });
  }
  /// Our login Methods will go here


}