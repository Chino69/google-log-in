import { Component } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {DomSanitizer} from '@angular/platform-browser'
import { Base64 } from '@ionic-native/base64/ngx';
import * as firebase from 'firebase'
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse,
  BackgroundGeolocationEvents
} from "@ionic-native/background-geolocation/ngx";
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
image: any;
lugar: any;
locLoop = 0;
  constructor(public imagePicker: ImagePicker, private backgroundGeolocation: BackgroundGeolocation, private camera: Camera, public router:Router, public afAuth: AngularFireAuth, private base64: Base64, public sanitizer: DomSanitizer) {}
  
  startBackgroundGeolocation() {
    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 1,
      distanceFilter: 1,
      debug: true, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false // enable this to clear background location settings when the app terminates
    };
    this.backgroundGeolocation.configure(config).then(() => {
      this.backgroundGeolocation
        .on(BackgroundGeolocationEvents.location)
        .subscribe((location: BackgroundGeolocationResponse) => {
          console.log(location);
          this.lugar = [];
          this.lugar.push ({altitude:location.altitude, latitude: location.latitude, longitude: location.longitude, time: location.time});
          firebase.database().ref('location/').push(this.lugar);
          this.locLoop++;


         

          // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
          // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
          // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
        });
    });
    this.backgroundGeolocation.start();
  }
  stopGPS(){
    this.backgroundGeolocation.stop();
    this.lugar = undefined;
    
  
  }
  

  
  openCam(){

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     //alert(imageData)
     this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
     firebase.database().ref('pictures/').push({image: this.image})
    }, (err) => {
     // Handle error
     alert("error "+JSON.stringify(err))
    });

  }
  async openImagePicker(){
    await this.imagePicker.hasReadPermission()
    .then(async (result) => {
      if(result == false){
        // no callbacks required as                    this opens a popup which returns async
        this.imagePicker.requestReadPermission();
      }
      else if(result == true){
      console.log("Ya tiene permisos Eso!");
      await this.imagePicker.getPictures({
       maximumImagesCount: 1
      }).then(
      async (results) => {
        for (var i = 0; i < results.length; i++) {
          const aux = this;

          const element = results[i];
          //await  this.uploadImageToFirebase(results[i]);
          let filePath: string = '';
          this.base64.encodeFile(element).then( async (base64File: string) => {
            console.log(base64File);
            this.image = this.sanitizer.bypassSecurityTrustUrl(base64File);
            console.log(this.image);
            console.log('Despues sigue firebase');
            firebase.database().ref('pictures/').push({image: aux.image}).then(sn => {
              console.log(sn.key, 'CARAJOOOOOOOOOOOO')
            })
           console.log('NO FUE FIREBASE');
          }, (err) => {
            console.log('ERROR PRIMERO')
            
          });  
          console.log('no se imagenes')
        console.log(results)
        
        }
               }, (err) => console.log('ERROR SEGUNDO', )
      );
      }
    }, (err) => {
      console.log(' ERROR, ultimo')
    });
  }
  logOut(){
    this.afAuth.auth.signOut();
    this.router.navigate(['/login'])
  }
  
}
